const https = require('https');
const server = https.createServer();
let port = process.env.PORT;
if (port == null || port == "") {
    port = 3000;
}
const fplApi = 'https://fantasy.premierleague.com/drf/bootstrap-static';

let playerData = [];

function createData (jsonData) {
    let tempData = [];
    
	for (let i = 0; i < jsonData.elements.length; i++) { 
        const price = parseFloat(jsonData.elements[i].now_cost);
        const elemType = parseInt(jsonData.elements[i].element_type);
        const minutes = parseInt(jsonData.elements[i].minutes);
        const points = parseInt(jsonData.elements[i].total_points);
        const ppg = parseFloat(jsonData.elements[i].points_per_game);
        const ppm = points / price;
        const ppgpm = ppg / (price / 10);
        const ppgmpm = points / minutes * 90 / (price / 10);
        const selected_by_percent = parseFloat(jsonData.elements[i].selected_by_percent);
        const market_cap = price * selected_by_percent; 
        const newPlayer = {
            "name": jsonData.elements[i].first_name + " " + jsonData.elements[i].web_name,
            "position": elemType,
            "price": price,
            "points": points,
            "ppg": ppg,
            "total_points": points,
            "minutes": minutes,
            "ppm": ppm,
            "ppgpm": ppgpm,
            "ppgmpm": ppgmpm,
            "selected_by_percent": selected_by_percent,
            "market_cap": market_cap
		}
        tempData.push(newPlayer);
    }
    playerData = tempData;
    console.log("Player data created");
}

const callFplApi = function() {
    console.log("Retrieving data from fpl api");
	https.get(fplApi, (resp) => {
		let data = '';

		// A chunk of data has been recieved.
		resp.on('data', (chunk) => {
			data += chunk;
		});

		// The whole response has been received. Print out the result.
		resp.on('end', () => {
			createData(JSON.parse(data));
		});

		}).on("error", (err) => {
			console.log("Error: " + err.message);
	});
}

function retrieveDataLoop() {
    callFplApi();
    setInterval(callFplApi, 3600000);
}

function parseRequest(req) {

}


server.on("request", (req, res) => {
    let body = [];
    console.log("request ", req.url);
    req.on("data", chunk => {
        body.push(chunk);
    });
    req.on("end", () => {
        body = body.concat.toString();
    });
    console.log()
    res.writeHead(200, {
        "Content-Type": "application/json"
    });
    res.write("hello");
    res.end();
    res.on("error", err => {
        console.log(err);
    });
    req.on("error", err => {
        console.log(err);
    });
});

server.listen(port, () => {
    retrieveDataLoop();
    console.log("Server listenting on port: " + port);
})

/* 
app.use(express.static(__dirname + "/public"));

function getData () {
	callFplApi();
    setInterval(callFplApi, 600000);
};

app.listen(port, () => {
    console.log(`Example app listening on port ${port}! Retrieving api data.`);
    getData();
});

app.get('/', (req, res) => {
    console.log("Index page requst received");
    res.sendFile(path.join(__dirname+'/index.html'));
});



app.get('/playerDataApi', (req, res) => {
    console.log("Player data api request received");
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(playerData));
});
 */