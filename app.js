const express = require('express');
const app = express();
const https = require('https');
const path = require('path');
const fplApi = 'https://fantasy.premierleague.com/drf/bootstrap-static'; //all data
const fplApiPlayerDetail = 'https://fantasy.premierleague.com/drf/element-summary/'; // + player id. history.total_points prop for previous
const qs = require('querystring');
const nodemailer = require('nodemailer');
const assert = require('assert');


app.use(express.static(__dirname + "/public"));

let port = process.env.PORT;
if (port == null || port == "") {
	port = 3000;
}

let playerData = [];


function getStdDev(playerData, playerPoints) {
    let total = 0;
    let stdDev = 0;
    let numGames = playerData.history.length;
    let meanPoints = playerPoints / numGames;
    if (playerData.hasOwnProperty("history")) {
        for (let i = 0; i < numGames; i++) {
            total += Math.pow((playerData.history[i].total_points - meanPoints), 2);
        }
        stdDev = Math.sqrt(total / numGames);
    }
    return stdDev;
}
/* 
function getSharpe(stdDev, ppm) {
    return ppm / stdDev;
} */

function createData (jsonData) {
    let newPlayerData = [];
    let numElements = jsonData.elements.length;
	for (let i = 0; i < numElements; i++) {
        //if (jsonData.elements[i].id == 247) {
        const id = parseInt(jsonData.elements[i].id);
        const price = parseFloat(jsonData.elements[i].now_cost);
        const elemType = parseInt(jsonData.elements[i].element_type);
        const minutes = parseInt(jsonData.elements[i].minutes);
        const points = parseInt(jsonData.elements[i].total_points);
        const ppg = parseFloat(jsonData.elements[i].points_per_game);
        const ppm = points / price;
        const ppgpm = ppg / (price / 10);
        const ppgmpm = points / minutes * 90 / (price / 10);
        const selected_by_percent = parseFloat(jsonData.elements[i].selected_by_percent);
        const market_cap = price * selected_by_percent;

        //get player specific detail
        let apiURL = fplApiPlayerDetail + id.toString();
        https.get(apiURL, (resp) => {
            let data = '';
    
            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });
    
            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                try {
                    data = JSON.parse(data);
                    const stdDev = getStdDev(data, points);
                    const sharpe = ((points - (data.history.length * 2)) / (price / 10)) / stdDev;
                    const newPlayer = {
                        "id": id,
                        "name": jsonData.elements[i].first_name + " " + jsonData.elements[i].web_name,
                        "position": elemType,
                        "price": price,
                        "points": points,
                        "ppg": ppg,
                        "total_points": points,
                        "minutes": minutes,
                        "ppm": ppm,
                        "ppgpm": ppgpm,
                        "ppgmpm": ppgmpm,
                        "selected_by_percent": selected_by_percent,
                        "market_cap": market_cap,
                        "std_dev": stdDev,
                        "sharpe": sharpe
                    }
                    newPlayerData.push(newPlayer);
                    numElements--;
                    if (numElements == 0) {
                        console.log("Player data created");
                        playerData = newPlayerData;
                    } else {
                        console.log("Elements data to be retrieved: " + numElements);
                    }
                } catch (parseErr) {
                    console.log("Error: " + parseErr);
                    numElements--;
                    if (numElements == 0) {
                        console.log("Player data created");
                        playerData = newPlayerData;
                    }
                }
            });
    
        }).on("error", (err) => {
                console.log("Error: " + err.message);
        });
    }
    //}
}

const callFplApi = function() {
	https.get(fplApi, (resp) => {
		let data = '';

		// A chunk of data has been recieved.
		resp.on('data', (chunk) => {
			data += chunk;
		});

		// The whole response has been received. Print out the result.
		resp.on('end', () => {
			createData(JSON.parse(data));
		});

		}).on("error", (err) => {
			console.log("Error: " + err.message);
	});
}


function getData () {
	callFplApi();
    setInterval(callFplApi, 3600000);
};

app.listen(port, () => {
    console.log(`Example app listening on port ${port}! Retrieving api data.`);
    getData();
});

app.get('/', (req, res) => {
    console.log("Index page requst received");
    res.sendFile(path.join(__dirname + '/html/index.html'));
});

app.get('/playerDataApi', (req, res) => {
    console.log("Player data api request received");
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(playerData));
});

app.get('/request-a-feature', (req, res) => {
    console.log("Request a feature request received");
    res.sendFile(path.join(__dirname + '/html/request-a-feature.html'));
});

app.post('/feature-request', (req, res) => {
    let body = "";
    req.on('data', (chunk) => {
        body += chunk;
        if (body.length > 1000000) {
            req.connection.destroy();
        }
    });
    req.on('end', () => {
        let post = qs.parse(body);
        res.sendFile(path.join(__dirname + '/html/thanks.html'));
        let transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                user: "fplbargains@gmail.com",
                pass: "mottie007"
            }
        });
        let mailOptions = {
            from: "fplbargains@gamil.com",
            to: "hilgardvr@gmail.com",
            subject: "new feature request",
            text: "From: " + post.emailAddress + "\nText: " + post.featureText
        }
        transporter.sendMail(mailOptions, (err, info) => {
            if (err) {
                console.log(err);
            } else {
                console.log("email send: " + info.response);
            }
        })
    });
});

app.get('/about', (req, res) => {
    console.log("About request received");
    res.sendFile(path.join(__dirname + '/html/about.html'));
});

app.get('/donate', (req, res) => {
    console.log("Donate request received");
    res.sendFile(path.join(__dirname + '/html/donate.html'));
});